# based on this article: https://mherman.org/blog/dockerizing-a-react-app/

FROM node as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY package-lock.json ./
RUN npm ci --silent
COPY . ./
RUN npm run build

# production environment
FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

# I used the following for development
#FROM node
#WORKDIR /app
#
#ENV APTH /app/node_modules/.bin:$PATH
#
#COPY package.json ./
#COPY package-lock.json /.
#RUN npm install --silent
#
#COPY . ./
#
#CMD ["npm", "start"]
